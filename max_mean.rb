# ------------------------------------------
# Solution with using ruby Array methods
#-------------------------------------------
class Max1
  attr_reader :items

  def initialize
    self.items = []
  end

  def push(item)
    items.push(item)
  end

  def pop
    items.pop
  end

  def max
    items.max
  end

  private

  attr_writer :items
end

class Mean1 < Max1
  def mean
    items.sum.to_f / items.length
  end
end

# ------------------------------------------
# Solution with using ruby Array methods + caching
#-------------------------------------------
class Max2
  attr_reader :items, :max

  def initialize
    self.items = []
    self.max = 0
  end

  def push(item)
    # recalculating max after each push 
    self.max = item if item > max
    items.push(item)
    item
  end

  def pop
    item = items.pop
    # recalculating max after each pop 
    self.max = get_max if item == max	
    item	
  end

  private

  attr_writer :items, :max

  def get_max
    items.max.to_i
  end
end

class Mean2 < Max2
  # for caching array length and mean
  attr_reader :length, :mean

  def initialize
    super
    self.length = 0
    self.mean = 0.0
  end

  def push(item)
    super
    # increasing length after each push
    self.length += 1
    # recalculating mean after each push by using old mean value
    self.mean = mean + item.to_f/length - mean/length
    item
  end

  def pop
    item = super
    # decreasing length after each pop
    self.length -= 1
    # recalculating mean after each pop by using old mean value
    self.mean = mean - item.to_f/length + mean/length
    item		
  end

  private

  attr_writer :length, :mean
end

# ------------------------------------------
# Solution with using Linked List + caching
#-------------------------------------------
class Max3
  class Node
    attr_accessor :value, :next_node

    def initialize(value, next_node)
      @value = value
      @next_node = next_node
    end
  end

  # caching max value
  attr_reader :max, :head

  def initialize
    self.head = nil
    self.max = 0
  end

  def push(item)
    # recalculating max after each push 
    self.max = item if item > max
    self.head = Node.new(item, head)
    item
  end

  def pop
    if head
      item = head.value
      self.head = head.next_node
      # recalculating max after each pop  
      self.max = get_max if item == max
      item
    end		
  end

  private 

  attr_writer :max, :head

  def get_max
    return 0 if head.nil? 
    ptr = head
    m = head.value
    while ptr.next_node do
      ptr = ptr.next_node
      m = ptr.value > m ? ptr.value : m
    end
    m   
  end
end

class Mean3 < Max3
  # for caching array length and mean
  attr_reader :length, :mean
  
  def initialize
    super
    self.length = 0
    self.mean = 0.0
  end

  def push(item)
    super
    # increasing length after each push
    self.length += 1
    # recalculating mean after each push by using old mean value
    self.mean = mean + item.to_f/length - mean/length
    item
  end

  def pop
    item = super
    if item
      # decreasing length after each pop
      self.length -= 1
      # recalculating mean after each pop by using old mean value
      self.mean = mean - item.to_f/length + mean/length
    end
    item		
  end
  
  private

  attr_writer :length, :mean
end

