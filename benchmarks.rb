require 'benchmark'
require 'benchmark/memory'
require 'benchmark/ips'
require_relative 'max_mean'

def benchmark_ips
  n = 100_000
  Benchmark.ips do |x|
    [3,2,1].each do |i|
      x.report("Mean#{i} ips") do 
        m = Object.const_get("Mean#{i}").new
        n.times do |j|
          m.push rand(n)
          m.max
          m.mean
          m.max
          m.mean
          m.pop if j % 25 == 0
          m.max
          m.mean
        end
      end
    end
    x.compare!
  end
end


def benchmark_memory
  n = 100_000
  Benchmark.memory do |x|
    [3,2,1].each do |i|
      x.report("Mean#{i} memory") do 
        m = Object.const_get("Mean#{i}").new
        n.times do |j|
          m.push rand(n)
          m.max
          m.mean
          m.max
          m.mean
          m.pop if j % 25 == 0
          m.max
          m.mean
        end
      end
    end
    x.compare!
  end
end

benchmark_ips
benchmark_memory

