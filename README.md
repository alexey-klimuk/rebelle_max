### Env:
ruby 2.4.0

### Installing env
`gem install bundler`

`bundle install`

### Running tests
`ruby unit_tests.rb`

### Running benchmarks
`ruby benchmarks.rb`

### Description
Ruby Coding Challenge

Create a class “Max” that exposes the following characteristics:

A public method called “push” that receives an unsigned integer number as parameter. This method should store the numbers internally while keeping the order they were pushed. We refer to this as the stack.
A public method called “pop”. When called it returns the integer that was last pushed to the stack or Nil if the stack is empty. The returned value is removed from the stack.
A public method called “max” which returns the highest number of the numbers in the stack.

The class will receive many “pushed” numbers (let’s say 10 million). The number of calls to “max” will be by magnitude greater than “push” or “pop” calls. Try to make „max” as fast as possible.

Create a class “Mean” which extends “Max”.

It should expose a method called “mean” which should return the mean value of the numbers in the stack. Again, make it as fast as possible.

Bonus Points: Your class now receives 10 trillion entries so the data won’t fit into memory. Talk a bit about how you would persist the data in a way that allows fast loading when recreating the class. How would you keep the functionality of the class when you don’t have access to all data any more.

### Benchmarks
Comparison:
```
Mean2 ips:       16.4 i/s

Mean3 ips:       10.7 i/s - 1.54x  slower

Mean1 ips:        0.0 i/s - 712.79x  slower
```

Comparison:
  
```
Mean1 memory:    1021752 allocated

Mean2 memory:   13501664 allocated - 13.21x more

Mean3 memory:   16480072 allocated - 16.13x more
```