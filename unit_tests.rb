require 'test/unit'
require_relative 'max_mean'

class TestMean < Test::Unit::TestCase
  def test_push
    (1..2).each do |i|
      m = Object.const_get("Mean#{i}").new
      m.push 2
      assert_equal([2], m.items)
      m.push 3
      assert_equal([2, 3], m.items)
    end
  end

  def test_pop
    (1..3).each do |i|
      m = Object.const_get("Mean#{i}").new
      m.push 3
      m.push 1
      assert_equal(1, m.pop)
      assert_equal(3, m.pop)
      assert_equal(nil, m.pop)
    end
  end

  def test_max
    (1..3).each do |i|
      m = Object.const_get("Mean#{i}").new
      [1,3,12,9].each { |i| m.push i }
      assert_equal(12, m.max)
      m.pop
      m.pop
      assert_equal(3, m.max)
    end
  end

  def test_mean
    (1..3).each do |i|
      m = Object.const_get("Mean#{i}").new
      [2,1,1].each { |i| m.push i }
      assert_equal(4/3.0, m.mean)
      m.pop
      assert_equal(3/2.0, m.mean)
    end
  end
end
